# Generated by Django 4.0.6 on 2022-07-21 05:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bookapp', '0007_remove_issue_genre_issue_genres'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='issue',
            name='genres',
        ),
        migrations.AddField(
            model_name='genre',
            name='magazines',
            field=models.ManyToManyField(related_name='magazine', to='bookapp.magazine'),
        ),
        migrations.AddField(
            model_name='issue',
            name='mag_title',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='issues', to='bookapp.magazine'),
        ),
    ]
