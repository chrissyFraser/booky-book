# Generated by Django 4.0.6 on 2022-07-20 19:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookapp', '0004_magazine'),
    ]

    operations = [
        migrations.RenameField(
            model_name='magazine',
            old_name='Release_Cycle',
            new_name='Cycle',
        ),
    ]
