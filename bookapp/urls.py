from django.contrib import admin
from django.urls import path, include
from bookapp.views import create_mag, delete_view, show_books, create_book, show_book, update_book, delete_view, show_mags, create_mag, show_mag, update_mag, delete_mag, show_genre, mag_genre, show_review, list_reviews


urlpatterns = [
    path("", show_books, name="show_books"),
    path("create/", create_book, name="create_book"),
    path("<int:pk>/", show_book, name="show_book"),
    path("<int:pk>/update/", update_book, name="update_book"),
    path("<int:pk>/delete/", delete_view, name="delete_view"),
    path("<int:pk>/review)", show_review, name="show_review"),
    path("reviews/", list_reviews, name="list_reviews"),
    
    path("mags/", show_mags, name="show_mags"),
    path("createmag/", create_mag, name="create_mag"),
    path("mag/<int:pk>/", show_mag, name="show_mag"),
    path("mag/<int:pk>/update/", update_mag, name="update_mag"),
    path("mag/<int:pk>/delete/", delete_mag, name="delete_mag"),
    path("mag/genre", show_genre, name="show_genre"),
    path("mag/genre/<int:pk>/", mag_genre, name="mag_genre"),    
]