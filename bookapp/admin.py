from django.contrib import admin
from bookapp.models import Book, Magazine, Issue, Genre, Author, BookReview


admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(Issue)
admin.site.register(Genre)
admin.site.register(Author)
admin.site.register(BookReview)