from django.shortcuts import render, redirect
from bookapp.forms import BookForm, MagForm
from bookapp.models import Book, Magazine, Genre, BookReview
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required

def show_books(request):
    booklist = Book.objects.all()
    context = {
        "books": booklist,
    }
    return render(request, "books/list.html", context)


def create_book(request):
    context = {}

    form = BookForm(request.POST or None)
    if form.is_valid():
        book = form.save()
        return redirect("show_book", pk=book.pk)
    
    context['form'] = form
    return render(request, "books/create.html", context)


def show_book(request, pk):
    context = {
        "book": Book.objects.get(pk=pk) if Book else None,
    }
    return render(request, "books/detail.html", context)


def update_book(request, pk):
    if Book and BookForm:
        instance = Book.objects.get(pk=pk)
        if request.method == "POST":
            form = BookForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("show_book", pk=pk)
        else:
            form = BookForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "books/update.html", context)


def delete_view(request, pk):
    
    context ={}
    
    obj = get_object_or_404(Book, pk = pk)
 
    if request.method == "POST":
        obj.delete()
        return redirect("show_books")
 
    return render(request, "books/delete_view.html", context)

def show_review(request, pk):
    review = BookReview.objects.get(pk=pk)

    context = {
        "review": review
    }
    return render(request, "book/review.html", context)


@login_required
def list_reviews(request):
    book_reviews_by_this_user = request.user.book_reviews_by_this_user.all()

    context = {
        "book_reviews_by_this_user": book_reviews_by_this_user
    }

    return render(request, "books/review_list.html", context)

#MAGAZINES

@login_required
def show_mags(request):
    maglist = request.user.mag.all()
    context = {
        "mags": maglist,
    }
    return render(request, "mag/list.html", context)

def create_mag(request):
    context = {}

    form = MagForm(request.POST or None)
    if form.is_valid():
        mag = form.save()
        return redirect("show_mag", pk=mag.pk)
    
    context['form'] = form
    return render(request, "mag/create.html", context)

def show_mag(request, pk):
    mag = Magazine.objects.get(pk=pk) if Magazine else None
    context = {
        "mag": mag,
    }
    return render(request, "mag/detail.html", context)

def update_mag(request, pk):
    if Magazine and MagForm:
        instance = Magazine.objects.get(pk=pk)
        if request.method == "POST":
            form = MagForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("show_mag", pk=pk)
        else:
            form = MagForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "mag/update.html", context)

def delete_mag(request, pk):
    
    context ={}
    
    obj = get_object_or_404(Magazine, pk = pk)
    if request.method == "POST":
        
        obj.delete()
        return redirect("show_mags")
    
    return render(request, "mag/delete_mag.html", context)
    
def show_genre(request):
    genre = Genre.objects.all()
    context = {
        "genre": genre
    }
    return render(request, "mag/genre.html", context)

def mag_genre(request, pk):
    spec_genre = Genre.objects.get(pk=pk)
    
    context = {
        "genre": spec_genre
    }
    return render(request, "mag/spec_genre.html", context)

