from django import forms
from .models import Book, Issue, Magazine


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        
        fields = [
            "title",
            "authors",
            "description",
            "published",
            "number_of_pages",
            "isbn_number",
            "image",
            "in_print",
        ]

class MagForm(forms.ModelForm):
    class Meta:
        model = Magazine
        
        fields = [
            "title",
            "image",
            "description",
            "cycle",
        ]

    class IssueForm(forms.ModelForm):
        class Meta:
            model = Issue

            fields = [
                "mag_title",
                "title",
                "issue_num",
                "issue_date",
                "cover_image",
                "description",
                "page_count",
            ]

    