from optparse import TitledHelpFormatter
from tkinter import CASCADE, Image
from django.db import models
from django.forms import URLField
from django.contrib.auth.models import User









class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name

class Book(models.Model):
    title = models.CharField(max_length=100)
    authors = models.ManyToManyField(Author, related_name="books")
    description = models.TextField(null=True, blank=True)
    published = models.SmallIntegerField(null=True, blank=True)
    number_of_pages = models.IntegerField(null=True, blank=True)
    isbn_number = models.IntegerField(null=True, blank=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True, blank=True)



    def __str__(self):
        return self.title + " by " + str(self.authors.first())



class BookReview(models.Model):
    reviewer = models.ForeignKey(User, related_name="book_reviews_by_this_user", on_delete=models.CASCADE)
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()


class Genre(models.Model):
    name = models.CharField(max_length=100, unique=True)
    books = models.ManyToManyField(Book, related_name="book", blank=True)
    def __str__(self):
        return self.name

class Magazine(models.Model):
    title = models.CharField(max_length=100)
    image = models.URLField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    cycle = models.CharField(max_length=20)
    genre = models.ManyToManyField(Genre, related_name="genre")
    creator = models.ForeignKey(User, related_name="mag", on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Issue(models.Model):
    mag_title = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=100)
    issue_num = models.SmallIntegerField(null=True, blank=True)
    issue_date = models.SmallIntegerField(null=True, blank=True)
    cover_image = models.URLField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True, blank=True)
    
    def __str__(self):
        return self.title



